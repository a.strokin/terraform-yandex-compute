# [2.0.0](https://gitlab.com/anton_patsev/terraform-yandex-compute/compare/v1.0.0...v2.0.0) (2022-10-19)


### breaking

* test ([cd4a611](https://gitlab.com/anton_patsev/terraform-yandex-compute/commit/cd4a611744de05487c1cbff3e64371119c1b4df4))

# 1.0.0 (2022-10-15)


### breaking

* new .releaserc.json ([2370fae](https://gitlab.com/anton_patsev/terraform-yandex-compute/commit/2370fae699ce14dff15d9cfd435e47441a0fff99))

### feat

* add conventional-changelog-conventionalcommits ([0f1aa99](https://gitlab.com/anton_patsev/terraform-yandex-compute/commit/0f1aa995992129fd6113cae7f1cd093a540e4108))
* add conventional-changelog-eslint ([65c9451](https://gitlab.com/anton_patsev/terraform-yandex-compute/commit/65c9451808f2c31b252060e1bc460c3020a30296))
* labels false ([bb567c8](https://gitlab.com/anton_patsev/terraform-yandex-compute/commit/bb567c8537962a3981360c91b24965284fb49690))
* npx semantic-release ([8cc5837](https://gitlab.com/anton_patsev/terraform-yandex-compute/commit/8cc5837067672a050039cc6976920ed5471ef8a4))

### image

* node:12-buster-slim ([1445e66](https://gitlab.com/anton_patsev/terraform-yandex-compute/commit/1445e6666e317320e873618fe8b9fb37683f6e7e))
* node:16-buster-slim ([c367398](https://gitlab.com/anton_patsev/terraform-yandex-compute/commit/c367398050e68589faaac1d920937f42e2a364ad))

### node

* 14-buster-slim ([cd1d669](https://gitlab.com/anton_patsev/terraform-yandex-compute/commit/cd1d669f16feca5389ce2a8d86a16913e451b54c))
* 16-buster-slim ([c918615](https://gitlab.com/anton_patsev/terraform-yandex-compute/commit/c918615f6d1795de0f05dbe9dd9b1d38a088a45b))

### refactor

* disable git config ([f952ed4](https://gitlab.com/anton_patsev/terraform-yandex-compute/commit/f952ed40347f595a5a435f063f8e190914ef2270))
